import java.util.function.IntPredicate;

public class StringCalculatorFiltering {

    public static IntPredicate higherThan1000(){
        return number -> number <= 1000;
    }

    public static IntPredicate negatives(){
        return number -> number < 0;
    }

}
