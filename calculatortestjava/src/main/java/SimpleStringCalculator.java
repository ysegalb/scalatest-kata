import java.util.function.Supplier;
import java.util.stream.IntStream;

public class SimpleStringCalculator {

    private StringCalculatorTokenizer tokenizer;

    public int add(String additionString) {

        tokenizer = new StringCalculatorTokenizer(additionString);

        if (hasNegativeNumbers(getNumbersStream())) {
            throw new RuntimeException(getNegativeNumbersMessage(getNumbersStream()));
        } else {
            return getNumbersStream().sum();
        }

    }

    private IntStream getNumbersStream() {
        Supplier<IntStream> numbers = () -> tokenizer.tokenize()
                    .filter(StringCalculatorFiltering.higherThan1000());
        return numbers.get();
    }

    private String getNegativeNumbersMessage(IntStream numbers) {
        StringBuilder error = new StringBuilder().append("Found negative number(s):");
        numbers.filter(StringCalculatorFiltering.negatives()).forEach(n -> error.append(" ").append(n));
        return error.toString();
    }

    private boolean hasNegativeNumbers(IntStream numbers) {
        return numbers.anyMatch(StringCalculatorFiltering.negatives());
    }

}
