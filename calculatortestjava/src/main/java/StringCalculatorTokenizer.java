import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class StringCalculatorTokenizer {

    private String numbers;
    private static String DEFAULT_DELIMITER = ";";
    private final String regex = "//(\\[.+\\])\\n[\\s\\S]+";
    private final Pattern pattern = Pattern.compile(regex);
    private final Matcher matcher;
    private List<String> delimiters = new ArrayList<>();

    public StringCalculatorTokenizer(String numbers) {
        this.matcher = pattern.matcher(numbers);
        this.numbers = numbers;
        getDelimiters(numbers);
    }

    public IntStream tokenize(){
        delimiters.forEach(this::replaceDelimiter);
        return Arrays.stream(numbers.split(DEFAULT_DELIMITER))
                .map(str -> "".equalsIgnoreCase(str) ? "0" : str)
                .mapToInt(Integer::parseInt);

    }

    private void replaceDelimiter(String delimiter){
        numbers = numbers.replaceAll(delimiter, DEFAULT_DELIMITER);
    }

    private void getDelimiters(String input){
        if (matcher.find()) {
            Arrays.stream(
                    matcher.group(1)
                            .replaceAll("\\[", "")
                            .replaceAll("\\]", ",")
                            .split(","))
                    .forEach(delimiter -> delimiters.add(delimiter));

            numbers = input.substring(matcher.end(1));
        } else {
            if (input.startsWith("//")) {
                delimiters.add(input.substring(2, 3));
                numbers = input.substring(4);
            } else {
                delimiters.add(",");
            }
        }
        numbers = numbers.replaceAll("\n", delimiters.get(0));
    }

}
