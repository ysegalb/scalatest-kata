import org.scalatest.WordSpec

class StringCalculatorTest extends WordSpec {

  val sut = new SimpleStringCalculator

  "A StringCalculator" when {
    "given an empty string as argument" should {
      "return 0" in {
        assert(this.sut.add("") == 0)
      }
    }
    "given only a number string (e.g. '1')" should {
      "return the same number ('1')" in {
        assert(this.sut.add("1") == 1)
      }
    }

    "given two numbers separated by comma (e.g. '1,2')" should {
      "return the addition of both ('3')" in {
        assert(this.sut.add("1,2") == 3)
      }
    }

    "given an unknown number of parameters (e.g. numbers from 1 to 5)" should{
      "return the sum of all of them (15)" in{
        assert(this.sut.add("1,2,3,4,5") == 15)
      }
    }

    "given \\n as number separator" should{
      "return the addition of them" in{
        assert(this.sut.add("1\n2,3") == 6)
      }
    }

    "given first line specifying delimiter" should{
      "treat the delimiter character for all input string" in{
        assert(this.sut.add("//;\n2;3;1") == 6)
      }
    }

    "given any input with negative numbers" should{
      "throw an exception informing the bad input" in{
        val exception = intercept[RuntimeException]{
          this.sut.add("//;\n3;-4;5;-2")
        }
        assert(exception.getMessage.equalsIgnoreCase("Found negative number(s): -4 -2"))
      }
    }

    "given any number greater than 1000" should{
      "ignore them in the final result" in{
        assert(this.sut.add("1001,2") == 2)
      }
    }

    "given any number lower or equal to 1000" should{
      "treat them in the final result" in{
        assert(this.sut.add("1000,2") == 1002)
      }
    }

    "given multiple delimiters" should{
      "apply all of them to the input" in{
        assert(this.sut.add("//[xx][vv]\n18xx7vv3") == 28)
      }
    }
  }
}
